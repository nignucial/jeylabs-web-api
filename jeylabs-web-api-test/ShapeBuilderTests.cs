﻿using System.Collections.Generic;
using jeylabs_web_api.Helpers;
using jeylabs_web_api.Shapes;
using Newtonsoft.Json;
using Xunit;

namespace jeylabs_web_api_test
{
    public class ShapeBuilderTests
    {
        private readonly ShapeBuilder _builder; 

        public ShapeBuilderTests()
        {
            _builder = new ShapeBuilder();
        }

        [Fact]
        void CreateShape_IsoscelesTriangleInfo_ReturnIsoscelesTriangle()
        {
            var expected = typeof(IsoscelesTriangle);

            var actual = _builder.CreateShape(new ShapeInfo()
            {
                Type = "isosceles triangle",
                Measurements = new List<Measurement>()
                {
                    new Measurement() {Type = "height", Amount = 200},
                    new Measurement() {Type = "width", Amount = 100}
                }
            }).GetType();

            Assert.Equal(expected, actual);
        }

        [Fact]
        void CreateShape_CircleInfo_ReturnCircle()
        {
            var expected = typeof(Circle);

            var actual = _builder.CreateShape(new ShapeInfo()
            {
                Type = "circle",
                Measurements = new List<Measurement>()
                {
                    new Measurement() {Type = "radius", Amount = 100}
                }
            }).GetType();

            Assert.Equal(expected, actual);
        }
    }
}