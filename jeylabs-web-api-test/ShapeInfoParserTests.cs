﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using jeylabs_web_api.Helpers;
using jeylabs_web_api.Shapes;
using Newtonsoft.Json;
using Xunit;

namespace jeylabs_web_api_test
{
    public class ShapeInfoParserTests
    {
        private readonly ShapeInfoParser _parser;

        public ShapeInfoParserTests()
        {
            _parser = new ShapeInfoParser();
        }

        [Fact]
        void Parse_CircleQuery_ReturnCircleInfo()
        {
            string query = "Draw a circle with a radius of 100";
            var expected = JsonConvert.SerializeObject(new ShapeInfo()
            {
                Type = "circle",
                Measurements = new List<Measurement>()
                {
                    new Measurement() {Type = "radius", Amount = 100}
                }
            });

            var actual = JsonConvert.SerializeObject(_parser.Parse(query));

            Assert.Equal(expected, actual);
        }

        [Fact]
        void Parse_SquareQuery_ReturnSqaureInfo()
        {
            string query = "Draw a square with a side length of 200";
            var expected = JsonConvert.SerializeObject(new ShapeInfo()
            {
                Type = "square",
                Measurements = new List<Measurement>()
                {
                    new Measurement() {Type = "side length", Amount = 200}
                }
            });

            var actual = JsonConvert.SerializeObject(_parser.Parse(query));

            Assert.Equal(expected, actual);
        }
        
        [Fact]
        void Parse_RectangleQuery_ReturnRectangleInfo()
        {
            string query = "Draw a rectangle with a width of 250 and a height of 400";
            var expected = JsonConvert.SerializeObject(new ShapeInfo()
            {
                Type = "rectangle",
                Measurements = new List<Measurement>()
                {
                    new Measurement() {Type = "width", Amount = 250},
                    new Measurement() {Type = "height", Amount = 400}
                }
            });

            var actual = JsonConvert.SerializeObject(_parser.Parse(query));

            Assert.Equal(expected, actual);
        }
        
        [Fact]
        void Parse_OctagonQuery_ReturnOctagonInfo()
        {
            string query = "Draw an octagon with a side length of 200";
            var expected = JsonConvert.SerializeObject(new ShapeInfo()
            {
                Type = "octagon",
                Measurements = new List<Measurement>()
                {
                    new Measurement() {Type = "side length", Amount = 200}
                }
            });

            var actual = JsonConvert.SerializeObject(_parser.Parse(query));

            Assert.Equal(expected, actual);
        }
        
        [Fact]
        void Parse_IsoscelesTriangleQuery_ReturnIsoscelesTriangleInfo()
        {
            string query = "Draw an isosceles triangle with a height of 200 and a width of 100";
            var expected = JsonConvert.SerializeObject(new ShapeInfo()
            {
                Type = "isosceles triangle",
                Measurements = new List<Measurement>()
                {
                    new Measurement() {Type = "height", Amount = 200},
                    new Measurement() {Type = "width", Amount = 100}
                }
            });

            var actual = JsonConvert.SerializeObject(_parser.Parse(query));

            Assert.Equal(expected, actual);
        }
    }
}
