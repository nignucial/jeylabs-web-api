﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using jeylabs_web_api.Helpers;
using jeylabs_web_api.Shapes;
using Xunit;

namespace jeylabs_web_api_test
{
    public class ShapeTests
    {
        [Fact]
        void ToSvg_IncompleteShape_ThrowException()
        {
            Shape shape = new IsoscelesTriangle();

            var exception = Record.Exception(() => XDocument.Load(new StringReader(shape.ToSvgXml())));
            Assert.NotNull(exception);
        }

        [Fact]
        void ToSvg_IsoscelesTriangle_ReturnValidSvgString()
        {
            Shape shape = new IsoscelesTriangle();
            shape.AddShapeInfo(new List<Measurement>()
            {
                new Measurement() {Type = "height", Amount = 200},
                new Measurement() {Type = "width", Amount = 100}
            });

            var exception = Record.Exception(() => XDocument.Load(new StringReader(shape.ToSvgXml())));
            Assert.Null(exception);
        }

        [Fact]
        void ToSvg_EquilateralTriangle_ReturnValidSvgString()
        {
            Shape shape = new EquilateralTriangle();
            shape.AddShapeInfo(new List<Measurement>()
            {
                new Measurement() {Type = "side length", Amount = 200},
            });

            var exception = Record.Exception(() => XDocument.Load(new StringReader(shape.ToSvgXml())));
            Assert.Null(exception);
        }

        [Fact]
        void ToSvg_Heptagon_ReturnValidSvgString()
        {
            Shape shape = new Heptagon();
            shape.AddShapeInfo(new List<Measurement>()
            {
                new Measurement() {Type = "side length", Amount = 200},
            });

            var exception = Record.Exception(() => XDocument.Load(new StringReader(shape.ToSvgXml())));
            Assert.Null(exception);
        }

        [Fact]
        void ToSvg_Hexagon_ReturnValidSvgString()
        {
            Shape shape = new Hexagon();
            shape.AddShapeInfo(new List<Measurement>()
            {
                new Measurement() {Type = "side length", Amount = 200},
            });

            var exception = Record.Exception(() => XDocument.Load(new StringReader(shape.ToSvgXml())));
            Assert.Null(exception);
        }

        [Fact]
        void ToSvg_Pentagon_ReturnValidSvgString()
        {
            Shape shape = new Pentagon();
            shape.AddShapeInfo(new List<Measurement>()
            {
                new Measurement() {Type = "side length", Amount = 200},
            });

            var exception = Record.Exception(() => XDocument.Load(new StringReader(shape.ToSvgXml())));
            Assert.Null(exception);
        }

        [Fact]
        void ToSvg_Square_ReturnValidSvgString()
        {
            Shape shape = new Square();
            shape.AddShapeInfo(new List<Measurement>()
            {
                new Measurement() {Type = "side length", Amount = 200},
            });

            var exception = Record.Exception(() => XDocument.Load(new StringReader(shape.ToSvgXml())));
            Assert.Null(exception);
        }

        [Fact]
        void ToSvg_Rectangle_ReturnValidSvgString()
        {
            Shape shape = new Rectangle();
            shape.AddShapeInfo(new List<Measurement>()
            {
                new Measurement() {Type = "height", Amount = 200},
                new Measurement() {Type = "width", Amount = 100}
            });

            var exception = Record.Exception(() => XDocument.Load(new StringReader(shape.ToSvgXml())));
            Assert.Null(exception);
        }

        [Fact]
        void ToSvg_Oval_ReturnValidSvgString()
        {
            Shape shape = new Oval();
            shape.AddShapeInfo(new List<Measurement>()
            {
                new Measurement() {Type = "height", Amount = 200},
                new Measurement() {Type = "width", Amount = 100}
            });

            var exception = Record.Exception(() => XDocument.Load(new StringReader(shape.ToSvgXml())));
            Assert.Null(exception);
        }

        [Fact]
        void ToSvg_Parallelogram_ReturnValidSvgString()
        {
            Shape shape = new Parallelogram();
            shape.AddShapeInfo(new List<Measurement>()
            {
                new Measurement() {Type = "height", Amount = 200},
                new Measurement() {Type = "width", Amount = 100}
            });

            var exception = Record.Exception(() => XDocument.Load(new StringReader(shape.ToSvgXml())));
            Assert.Null(exception);
        }

        [Fact]
        void ToSvg_ScaleneTriangle_ReturnValidSvgString()
        {
            Shape shape = new ScaleneTriangle();
            shape.AddShapeInfo(new List<Measurement>()
            {
                new Measurement() {Type = "height", Amount = 200},
                new Measurement() {Type = "width", Amount = 100}
            });

            var exception = Record.Exception(() => XDocument.Load(new StringReader(shape.ToSvgXml())));
            Assert.Null(exception);
        }

        [Fact]
        void ToSvg_Circle_ReturnValidSvgString()
        {
            Shape shape = new Circle();
            shape.AddShapeInfo(new List<Measurement>()
            {
                new Measurement() {Type = "radius", Amount = 200},
            });

            var exception = Record.Exception(() => XDocument.Load(new StringReader(shape.ToSvgXml())));
            Assert.Null(exception);
        }
    }
}