﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using jeylabs_web_api.Controllers;
using jeylabs_web_api.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Xunit;

namespace jeylabs_web_api_test
{
    public class ShapesControllerTests
    {
        private readonly ShapesController _controller;

        public ShapesControllerTests()
        {
            _controller = new ShapesController();
        }

        [Fact]
        void Get_OctagonQuery_ReturnValidSvgXmlResponse()
        {
            string query = "Draw an octagon with a side length of 200";
            var result = _controller.Get(query);

            var contentResult = Assert.IsType<ContentResult>(result);
            var exception = Record.Exception(() => XDocument.Load(new StringReader(contentResult.Content)));
            Assert.Null(exception);
        }
    }
}