﻿using System;
using System.Linq;
using System.Threading.Tasks;
using jeylabs_web_api.Helpers;
using jeylabs_web_api.Shapes;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace jeylabs_web_api.Controllers
{
    [Produces("application/xml")]
    [Route("api/Shapes")]
    public class ShapesController : Controller
    {
        // GET: api/Shapes
        [HttpGet]
        public IActionResult Get(string query)
        {
            var parser = new ShapeInfoParser();
            ShapeInfo shapeInfo = parser.Parse(query);

            var builder = new ShapeBuilder();
            Shape shape = builder.CreateShape(shapeInfo);

            return Content(shape.ToSvgXml(), "application/xml");
        }
    }
}
