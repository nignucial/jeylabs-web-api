﻿namespace jeylabs_web_api.Helpers
{
    public class Measurement
    {
        public string Type { get; set; }
        public float Amount { get; set; }
    }
}
