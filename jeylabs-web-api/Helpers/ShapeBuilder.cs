﻿using jeylabs_web_api.Shapes;
using System;
using System.Collections.Generic;

namespace jeylabs_web_api.Helpers
{
    public class ShapeBuilder
    {
        private readonly Dictionary<string, Func<Shape>> _shapeBuildingDictionary =
            new Dictionary<string, Func<Shape>>()
            {
                {"isosceles triangle", () => new IsoscelesTriangle()},
                {"square", () => new Square()},
                {"scalene triangle", () => new ScaleneTriangle()},
                {"parallelogram", () => new Parallelogram()},
                {"equilateral triangle", () => new EquilateralTriangle()},
                {"pentagon", () => new Pentagon()},
                {"rectangle", () => new Rectangle()},
                {"hexagon", () => new Hexagon()},
                {"heptagon", () => new Heptagon()},
                {"octagon", () => new Octagon()},
                {"circle", () => new Circle()},
                {"oval", () => new Oval()},
            };

        public Shape CreateShape(ShapeInfo shapeInfo)
        {
            Shape shape = _shapeBuildingDictionary[shapeInfo.Type]();
            shape.AddShapeInfo(shapeInfo.Measurements);
            
            return shape;
        }
    }
}