﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jeylabs_web_api.Helpers
{
    public class ShapeInfo
    {
        public ShapeInfo()
        {
            Measurements = new List<Measurement>();
        }

        public string Type { get; set; }
        public IEnumerable<Measurement> Measurements { get; set; }
    }
}
