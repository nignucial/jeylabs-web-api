﻿using System;
using System.Collections.Generic;

namespace jeylabs_web_api.Helpers
{
    public class ShapeInfoParser
    {
        //  Could use regex?
        public ShapeInfo Parse(string query)
        {
            var words = query.Split(' ');

            string shapeType = GetShapeType(words, out int withIndex);
            List<Measurement> measurements = GetMeasurements(words, withIndex);

            return new ShapeInfo()
            {
                Type = shapeType,
                Measurements = measurements
            };
        }

        private List<Measurement> GetMeasurements(string[] words, int withIndex)
        {
            var measurements = new List<Measurement>();

            int i = withIndex;
            while (i < words.Length)
            {
                int measurementTypeStartIndex = i + 2;
                string measurementType = words[measurementTypeStartIndex];
                float amount = default(float);

                for (int j = measurementTypeStartIndex + 1; j < words.Length; j++)
                {
                    string word = words[j];
                    if (word != "of")
                    {
                        measurementType += $" {word}";
                    }
                    else
                    {
                        amount = float.Parse(words[j + 1]);
                        i = j + 2;
                        break;
                    }
                }

                measurements.Add(new Measurement()
                {
                    Type = measurementType,
                    Amount = amount
                });
            }

            return measurements;
        }

        private string GetShapeType(string[] words, out int withIndex)
        {
            withIndex = default(int);
            const int shapeStartIndex = 2;

            string shapeType = words[shapeStartIndex];
            for (int index = shapeStartIndex + 1; index < words.Length; index++)
            {
                string word = words[index];
                if (word != "with")
                {
                    shapeType += $" {word}";
                }
                else
                {
                    withIndex = index;
                    break;
                }
            }

            return shapeType;
        }
    }
}
