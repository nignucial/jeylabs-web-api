﻿using System;
using System.Collections.Generic;
using System.Linq;
using jeylabs_web_api.Helpers;
using Svg;

namespace jeylabs_web_api.Shapes
{
    public class Circle : Shape
    {
        private float? _radius;

        private float Radius
        {
            get => _radius.Value;
            set
            {
                if (value <= 0) throw new ArgumentOutOfRangeException(nameof(value));
                _radius = value;
            }
        }

        public override void AddShapeInfo(IEnumerable<Measurement> measurements)
        {
            Radius = measurements.Single(measurement => measurement.Type == "radius").Amount;
        }

        protected override SvgVisualElement ToSvgShape()
        {
            return new SvgCircle()
            {
                Radius = Radius,
                CenterX = Radius,
                CenterY = Radius,
            };
        }
    }
}