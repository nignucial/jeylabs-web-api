﻿using System;
using System.Collections.Generic;
using System.Linq;
using Svg;

namespace jeylabs_web_api.Shapes
{
    public class EquilateralTriangle : RegularPolygon
    {
        public EquilateralTriangle()
        {
            NoSide = 3;
        }
    }
}