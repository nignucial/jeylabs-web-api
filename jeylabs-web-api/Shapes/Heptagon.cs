﻿namespace jeylabs_web_api.Shapes
{
    public class Heptagon : RegularPolygon
    {
        public Heptagon()
        {
            NoSide = 7;
        }
    }
}