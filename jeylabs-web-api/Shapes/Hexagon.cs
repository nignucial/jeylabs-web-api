﻿namespace jeylabs_web_api.Shapes
{
    public class Hexagon : RegularPolygon
    {
        public Hexagon()
        {
            NoSide = 6;
        }
    }
}