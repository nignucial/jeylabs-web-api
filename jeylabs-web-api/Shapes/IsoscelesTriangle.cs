﻿using System;
using System.Collections.Generic;
using System.Linq;
using jeylabs_web_api.Helpers;
using Svg;

namespace jeylabs_web_api.Shapes
{
    public class IsoscelesTriangle : Shape
    {
        private float? _height;
        private float? _width;

        private float Height
        {
            get => _height.Value;
            set
            {
                if (value <= 0) throw new ArgumentOutOfRangeException(nameof(value));   
                _height = value;
            }
        }

        private float Width
        {
            get => _width.Value;
            set
            {
                if (value <= 0) throw new ArgumentOutOfRangeException(nameof(value));
                _width = value;
            }
        }

        public override void AddShapeInfo(IEnumerable<Measurement> measurements)
        {
            foreach (Measurement measurement in measurements)
            {
                switch (measurement.Type)
                {
                    case "width":
                        Width = measurement.Amount;
                        break;

                    case "height":
                        Height = measurement.Amount;
                        break;
                }
            }
        }

        protected override SvgVisualElement ToSvgShape()
        {
            var svgShape = new SvgPolygon()
            {
                Points = new SvgPointCollection()
                {
                    0,
                    Height,

                    Width,
                    Height,

                    Width / 2,
                    0,
                }
            };

            #region Svg Library Bug Workaround

            var noopUri = new Uri("http://localhost");
            svgShape.MarkerStart = noopUri;
            svgShape.MarkerMid = noopUri;
            svgShape.MarkerEnd = noopUri;

            #endregion

            return svgShape;
        }
    }
}
