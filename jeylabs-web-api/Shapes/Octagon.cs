﻿namespace jeylabs_web_api.Shapes
{
    public class Octagon : RegularPolygon
    {
        public Octagon()
        {
            NoSide = 8;
        }
    }
}