﻿using System;
using System.Collections.Generic;
using jeylabs_web_api.Helpers;
using Svg;

namespace jeylabs_web_api.Shapes
{
    public class Oval : Shape
    {
        private float? _height;
        private float? _width;

        private float Height
        {
            get => _height.Value;
            set
            {
                if (value <= 0) throw new ArgumentOutOfRangeException(nameof(value));
                _height = value;
            }
        }

        private float Width
        {
            get => _width.Value;
            set
            {
                if (value <= 0) throw new ArgumentOutOfRangeException(nameof(value));
                _width = value;
            }
        }

        public override void AddShapeInfo(IEnumerable<Measurement> measurements)
        {
            foreach (Measurement measurement in measurements)
            {
                switch (measurement.Type)
                {
                    case "width":
                        Width = measurement.Amount;
                        break;

                    case "height":
                        Height = measurement.Amount;
                        break;
                }
            }
        }

        protected override SvgVisualElement ToSvgShape()
        {
            float radiusX = Width / 2;
            float radiusY = Height / 2;

            return new SvgEllipse()
            {
                RadiusX = radiusX,
                RadiusY = radiusY,
                CenterX = radiusX,
                CenterY = radiusY,
            };
        }
    }
}