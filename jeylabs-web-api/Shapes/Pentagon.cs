﻿using System;
using System.Collections.Generic;
using System.Linq;
using jeylabs_web_api.Helpers;
using Svg;

namespace jeylabs_web_api.Shapes
{
    public class Pentagon : RegularPolygon
    {
        public Pentagon()
        {
            NoSide = 5;
        }
    }
}