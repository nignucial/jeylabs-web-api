﻿using System;
using System.Collections.Generic;
using jeylabs_web_api.Helpers;
using Svg;

namespace jeylabs_web_api.Shapes
{
    public class Rectangle : Shape
    {
        private float? _height;
        private float? _width;

        private float Height
        {
            get => _height.Value;
            set => _height = value;
        }

        private float Width
        {
            get => _width.Value;
            set => _width = value;
        }

        public override void AddShapeInfo(IEnumerable<Measurement> measurements)
        {
            foreach (Measurement measurement in measurements)
            {
                switch (measurement.Type)
                {
                    case "width":
                        Width = measurement.Amount;
                        break;

                    case "height":
                        Height = measurement.Amount;
                        break;
                }
            }
        }

        protected override SvgVisualElement ToSvgShape()
        {
            return new SvgRectangle()
            {
                Width = Width,
                Height = Height
            };
        }
    }
}