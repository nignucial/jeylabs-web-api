﻿using System;
using System.Collections.Generic;
using System.Linq;
using jeylabs_web_api.Helpers;
using Svg;

namespace jeylabs_web_api.Shapes
{
    public abstract class RegularPolygon : Shape
    {
        protected long NoSide
        {
            private get => _noSide.Value;
            set
            {
                if (value < 3)
                    throw new ArgumentOutOfRangeException(nameof(value));

                _noSide = value;
            }
        }

        private float SideLength
        {
            get => _sideLength.Value;
            set
            {
                if (value <= 0) throw new ArgumentOutOfRangeException(nameof(value));
                _sideLength = value;
                }
        }

        private long? _noSide;
        private float? _sideLength;

        public sealed override void AddShapeInfo(IEnumerable<Measurement> measurements)
        {
            SideLength = measurements.Single(measurement => measurement.Type == "side length").Amount;
        }

        protected sealed override SvgVisualElement ToSvgShape()
        {
            float angle = 2 * MathF.PI / NoSide;
            float radius = SideLength / 2 / MathF.Sin(MathF.PI / 180f * 180f / NoSide);
            var centerX = radius;
            var centerY = radius;

            var points = new SvgPointCollection();
            for (int i = 0; i < NoSide; i++)
            {
                float pointX = centerX + radius * MathF.Sin(i * angle);
                float pointY = centerY + radius * MathF.Cos(i * angle);
                points.Add(pointX);
                points.Add(pointY);
            }

            var svgShape = new SvgPolygon()
            {
                Points = points
            };

            #region Svg Library Bug Workaround

            var noopUri = new Uri("http://localhost");
            svgShape.MarkerStart = noopUri;
            svgShape.MarkerMid = noopUri;
            svgShape.MarkerEnd = noopUri;

            #endregion

            return svgShape;
        }
    }
}