﻿using System;
using System.Collections.Generic;
using jeylabs_web_api.Helpers;
using Svg;

namespace jeylabs_web_api.Shapes
{
    public class ScaleneTriangle : Shape
    {
        private float? _height;
        private float? _width;

        private float Height
        {
            get => _height.Value;
            set => _height = value;
        }

        private float Width
        {
            get => _width.Value;
            set => _width = value;
        }

        public override void AddShapeInfo(IEnumerable<Measurement> measurements)
        {
            foreach (Measurement measurement in measurements)
            {
                switch (measurement.Type)
                {
                    case "width":
                        Width = measurement.Amount;
                        break;

                    case "height":
                        Height = measurement.Amount;
                        break;
                }
            }
        }

        protected override SvgVisualElement ToSvgShape()
        {
            var svgShape = new SvgPolygon()
            {
                Points = new SvgPointCollection()
                {
                    0,
                    Height,

                    Width,
                    Height,
                }
            };
            if (Math.Abs(Height - Width) < 1f)
            {
                svgShape.Points.Add(Width * 1.5f);
                svgShape.Points.Add(0);
            }
            else
            {
                svgShape.Points.Add(Width);
                svgShape.Points.Add(0);
            }

            #region Svg Library Bug Workaround

            var noopUri = new Uri("http://localhost");
            svgShape.MarkerStart = noopUri;
            svgShape.MarkerMid = noopUri;
            svgShape.MarkerEnd = noopUri;

            #endregion

            return svgShape;
        }
    }
}