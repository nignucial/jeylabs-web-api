﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using jeylabs_web_api.Helpers;
using Svg;

namespace jeylabs_web_api.Shapes
{
    public abstract class Shape
    {
        public abstract void AddShapeInfo(IEnumerable<Measurement> measurements);
        protected abstract SvgVisualElement ToSvgShape();

        public string ToSvgXml()
        {
            var svgShape = ToSvgShape();

            var viewBox = GetViewBox(svgShape);

            var document = new SvgDocument()
            {
                Width = viewBox.Width,
                Height = viewBox.Height,
                ViewBox = viewBox,
                Children = {svgShape}
            };

            #region Svg Library Bug Workaround

            var noopUri = new Uri("http://localhost");
            svgShape.ClipPath = noopUri;
            svgShape.Filter = noopUri;

            #endregion

            string xml = document.GetXML();
            return xml;
        }

        private SvgViewBox GetViewBox(SvgVisualElement svgShape)
        {
            var minX = float.MaxValue;
            var minY = float.MaxValue;
            var maxX = float.MinValue;
            var maxY = float.MinValue;

            foreach (var pathPoint in svgShape.Path(SvgRenderer.FromNull()).PathPoints)
            {
                minX = Math.Min(pathPoint.X, minX);
                maxX = Math.Max(pathPoint.X, maxX);
                minY = Math.Min(pathPoint.Y, minY);
                maxY = Math.Max(pathPoint.Y, maxY);
            };

            return new SvgViewBox(minX, minY, maxX, maxY);
        }
    }
}
