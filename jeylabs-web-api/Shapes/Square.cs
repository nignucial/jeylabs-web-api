﻿using System.Collections.Generic;
using System.Linq;
using jeylabs_web_api.Helpers;
using Svg;

namespace jeylabs_web_api.Shapes
{
    public class Square : Shape
    {
        private float? _sideLength;

        private float SideLength
        {
            get => _sideLength.Value;
            set => _sideLength = value;
        }

        public override void AddShapeInfo(IEnumerable<Measurement> measurements)
        {
            SideLength = measurements.Single(measurement => measurement.Type == "side length").Amount;
        }

        protected override SvgVisualElement ToSvgShape()
        {
            return new SvgRectangle()
            {
                Width = SideLength,
                Height = SideLength
            };
        }
    }
}